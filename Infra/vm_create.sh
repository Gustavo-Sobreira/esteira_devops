#!/bin/sh

# Função para imprimir o logotipo
printLogo() {
    clear
    echo '=====  =====  =====  =====  =====  =====  =====  ====='
    echo '    ::::::::::::::::::::::::::::::::::::::::::::::'
    echo '    :::::::::::::::::::::     ::::::::::::::::::::'
    echo '    :::::::::::::::::::: :::::::::::::::::::::::::'
    echo '    :::::::::::::::::::::    :::::::::::::::::::::'
    echo '    ::::::::::::::::::::::::: ::::::::::::::::::::'
    echo '    ::::::::::::::::::::     :::::::::::::::::::::'
    echo '=====  =====  =====  =====  =====  =====  =====  ====='
}

# Função para perguntar se o usuário deseja criar máquinas virtuais e quantas
perguntarQuantidade() {
    echo ' - Deseja criar uma máquina virtual (s/n)? [s]'
    read DESEJO
    DESEJO=${DESEJO:-s}

    if [ "$DESEJO" = "n" ]; then
        echo 'Saindo do programa.'
        exit 0
    fi

    echo ' - Quantas máquinas deseja criar? [3]'
    read TOTALdeMAQUINAS
    TOTALdeMAQUINAS=${TOTALdeMAQUINAS:-3}
}

# Função para criar uma máquina virtual
criarMaquina() {
    tipo=$1
    nome_default=$2
    ram_default=$3
    disk_default=$4
    cpu_default=$5

    echo " - Qual nome para sua máquina? [$nome_default]"
    read NOME
    NOME=${NOME:-$nome_default}

    echo " - Total de memória RAM? [$ram_default]"
    read RAM
    RAM=${RAM:-$ram_default}

    echo " - Espaço de disco alocado? [$disk_default]"
    read DISK
    DISK=${DISK:-$disk_default}

    echo " - Total de CPUs? [$cpu_default]"
    read CPU
    CPU=${CPU:-$cpu_default}

    echo "Criando a máquina virtual $tipo com as seguintes configurações:"
    echo "Nome: $NOME"
    echo "Memória RAM: $RAM"
    echo "Espaço de Disco: $DISK"
    echo "CPUs: $CPU"

    # Adicionando mensagens de debug
    echo "Comando: multipass launch --name \"$NOME\" --memory \"$RAM\" --disk \"$DISK\" --cpus \"$CPU\""

    # Lançar o comando multipass em um novo terminal
    gnome-terminal -- bash -c "multipass launch --name \"$NOME\" --memory \"$RAM\" --disk \"$DISK\" --cpus \"$CPU\"; exec bash"
}

# Função principal
main() {
    printLogo
    perguntarQuantidade

    # Criar a máquina Master
    criarMaquina "Master" "Master" "2G" "15G" "2"

    # Criar máquinas adicionais
    for i in $(seq 1 $((TOTALdeMAQUINAS-1))); do
        printLogo
        criarMaquina "Worker" "Worker$i" "2G" "10G" "1"
    done

    echo "Todos os terminais para a criação de máquinas foram abertos!"
}

# Executar a função principal
main
