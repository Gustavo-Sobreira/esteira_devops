	#!/bin/sh

	ROOT_NAME="root"
	MENSAGE_ROOT="[ ALERT ] - Favor execute como root (sudo su)"

	verify_user(){
		if [ "$USER" != "$ROOT_NAME" ]; then
		    echo $MENSAGE_ROOT
		    echo " -> Usuário atual: $USER"
		    read ""PRESS
		    exit
		fi
	}

	update_all(){
		echo "Buscando atualizacoes..."
		apt update 
		clear

		echo "Instalando atualizações..."
		apt upgrade -y
		clear
	}

	install_docker(){
		echo "Limpando instalações antigas:"
		for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
		
		sudo apt install docker.io -y
		clear 

		echo "Verificando se instalacao concluída com sucesso..."
		docker -v
		
		echo "Usuário para execução do docker: "
		read ""USER_NAME
		usermod -aG docker $USER_NAME
	}

	verify_user
	update_all
	install_docker
