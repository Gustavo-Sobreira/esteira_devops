Cypress.on('test:after:run', (test, runnable) => {
  if (test.state === 'failed') {
    let path = Cypress.spec.relative 
    let modifiedPath = path.split('/')
    modifiedPath = modifiedPath.slice(3, -1)
  }
})

Cypress.on('uncaught:exception', (err, runnable) => {
  return false
})

