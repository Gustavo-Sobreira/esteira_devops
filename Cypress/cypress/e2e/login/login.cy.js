describe('Pagina_Home', () => {
    beforeEach(() => {
        cy.visit('/')
    });

    it('Deve inserir login e senha com sucesso', () => {
        cy.get('body').click()

        cy.get('#formLogin\\:txtLogin').as('login')
        verificarCampoValido('@login')
        cy.get('@login').type('ze@ze.com.br')
        
        cy.get('#formLogin\\:txtSenha').as('senha')
        verificarCampoValido('@senha')
        cy.get('@senha').type(123456)

        cy.get('#formLogin\\:j_idt17 > .ui-button-text').click()
    });


    it('Deve cadastrar novo aluno', () => {
        cy.get('#formLogin\\:j_idt18 > .ui-button-text')

        cy.url().should('eq', 'http://10.171.226.164:30081/enade/cadastroAluno.xhtml')
    })

    it('Deve abrir a leta de cadastro', () => {
        cy.visit('/cadastroAluno.xhtml')
        cy.url().should('eq', 'http://10.171.226.164:30081/enade/cadastroAluno.xhtml')
    });
    
});

const verificarCampoValido = (campo) => {
    cy.get(campo)
            .should('be.visible')
            .and('not.be.disabled')
            .clear()
}