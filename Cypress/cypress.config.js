const { defineConfig } = require("cypress");
const cypressEnv = require('./cypress.env.json');

ip = cypressEnv.ip
port = cypressEnv.port
sub_dominio = cypressEnv.sub_dominio

module.exports = defineConfig({
  e2e: {
    baseUrl: `http://${ip}:${port}/${sub_dominio}/`,
    env: {
      requestMode: true,
      snapshotOnly: true,
    },
    experimentalSessionAndOrigin: true,
  },
  pageLoadTimeout: 120000,
  defaultCommandTimeout: 7000,
  viewportWidth: 1280,
  viewportHeight: 720,
  reporter: "mochawesome",
  reporterOptions: {
    reportDir: "mochawesome-report",
    reportFilename: "index.html",
    overwrite: true,
    html: true,
    json: true
  },
  screenshotsFolder: "mochawesome-report/assets/screenshots",
  video: false,

  retries: {
    runMode: 1,
    openMode: 0
  }
});
