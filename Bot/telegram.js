const fs = require('fs');
const axios = require('axios');
const TelegramBot = require('node-telegram-bot-api');
const express = require('express');

const token = '7097557506:AAFkm_nWoj4F4alvQHlxK7ActjbjroMlCnU';
const chatId = '-4245740079';

const bot = new TelegramBot(token, { polling: true });
const app = express();
const port = 3000;

const reportUrl = 'http://cypress:80/index.json';

function formatDateTime(dateTime) {
    const date = new Date(dateTime);
    const formattedDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} - ${date.getHours()}:${date.getMinutes()}`;
    return formattedDate;
}

function sendMessage(stats) {
    const message = `   
-----------------------   🚜   -----------------------

🤠 - Aoba bão?
🤠 - Os trem tão pronto!

RELATÓRIO DE TESTES ENADE
-----------------------   📑   -----------------------

🕙 - TEMPO

    Inicio: ${formatDateTime(stats.start)}
    Término: ${formatDateTime(stats.end)}
    Duração:  ${stats.duration / 1000} seg.

🧪 - TESTES

    Total: ${stats.tests}
    Taxa Sucesso: ${stats.passPercent}%

    🟢 - Sucesso: ${stats.passes}
    🟡 - Pendencias: ${stats.pending}
    🔴 - Erros: ${stats.failures}
    ⚪ - Pulados: ${stats.skipped}
    🔘 - Outros: ${stats.other}

-----------------------   📑   -----------------------

🤠 - TCHAU BRIGADO!`;
    bot.sendMessage(chatId, message);
}

async function readMochawesomeReport() {
    console.log("Buscando dados do mocha");
    try {
        const response = await axios.get(reportUrl);
        const report = response.data;

        const stats = report.stats || {};
        sendMessage(stats);
    } catch (error) {
        console.error('Erro ao ler o arquivo de relatório:', error.message);
    }
}

app.get('/test', async (req, res) => {
    await readMochawesomeReport();
    res.send('Relatório enviado com sucesso!');
});

app.listen(port, () => {
    console.log(`Servidor rodando na porta ${port}`);
});

setInterval(readMochawesomeReport, 3600000);
readMochawesomeReport();
