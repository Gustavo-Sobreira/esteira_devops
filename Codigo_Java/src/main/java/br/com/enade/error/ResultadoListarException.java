package br.com.enade.error;

public class ResultadoListarException extends RuntimeException {
    public ResultadoListarException(String message, Throwable cause) {
        super(message, cause);
    }
}
