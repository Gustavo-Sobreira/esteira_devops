package br.com.enade.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class DAO<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Class<T> classe;
    private final EntityManager em;

    public DAO(EntityManager em, Class<T> classe) {
        this.em = em;
        this.classe = classe;
    }

    public void adiciona(T entity) {
        em.persist(entity);
    }

    public void remove(T entity) {
        em.remove(em.merge(entity));
    }

    public void atualiza(T entity) {
        em.merge(entity);
    }

    public List<T> listaTodos() {
        CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
        query.select(query.from(classe));
        return em.createQuery(query).getResultList();
    }

    public T buscaPorId(Long id) {
        return em.find(classe, id);
    }

    public long contaTodos() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<T> root = criteriaQuery.from(classe);
        criteriaQuery.select(criteriaBuilder.count(root));

        return em.createQuery(criteriaQuery).getSingleResult();
    }


    public List<T> listaTodosPaginada(int firstResult, int maxResults) {
        CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
        query.select(query.from(classe));
        return em.createQuery(query)
                .setFirstResult(firstResult)
                .setMaxResults(maxResults)
                .getResultList();
    }
}
