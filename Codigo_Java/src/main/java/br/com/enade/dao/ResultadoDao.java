package br.com.enade.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.enade.model.Tbresultado;

public class ResultadoDao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    EntityManager em;

    private DAO<Tbresultado> dao;

    @PostConstruct
    public void init() {
        this.dao = new DAO<>(this.em, Tbresultado.class);
    }

    public Tbresultado buscarPorId(Long id) {
        return this.dao.buscaPorId(id);
    }

    public void adiciona(Tbresultado resultado) {
        this.dao.adiciona(resultado);
    }

    public void remove(Tbresultado resultado) {
        this.dao.remove(resultado);
    }

    public List<Tbresultado> listarTodos() {
        return this.dao.listaTodos();
    }

    public void atualiza(Tbresultado resultado) {
        this.dao.atualiza(resultado);
    }
}
