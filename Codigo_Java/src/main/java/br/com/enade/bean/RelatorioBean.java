package br.com.enade.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.enade.util.Relatorio;


@Named
@ViewScoped
public class RelatorioBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String relatorio;
    private final Map<String, String> tipos;

    public RelatorioBean() {
        tipos = new HashMap<>();
        tipos.put("Relatorio de Alunos", "ListaAlunos");
        tipos.put("Relatorio de Alunos X Notas", "AlunoXNota");
        tipos.put("Relatorio de Alunos sem Prova", "AlunosPendentesSemProva");
        tipos.put("Relatorio de Alunos com Prova Pendente", "AlunosPendentesComProva");
    }


    public String getRelatorio() {
        return relatorio;
    }

    public void setRelatorio(String relatorio) {
        this.relatorio = relatorio;
    }

    public Map<String, String> getTipos() {
        return tipos;
    }

    public void gerarRelatorio() {
        Relatorio geradorRelatorio = new Relatorio();
        geradorRelatorio.getRelatorio(this.relatorio);
    }
}
