package br.com.enade.bean;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;

import br.com.enade.dao.TipoUsuarioDao;
import br.com.enade.dao.UsuarioDao;
import br.com.enade.model.Tbtipousuario;
import br.com.enade.model.Tbusuario;
import br.com.enade.tx.Transacional;
import br.com.enade.util.Relatorio;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private Tbusuario usuario;

    @Inject
    private UsuarioDao dao;

    @Inject
    private TipoUsuarioDao tipoUsuarioDao;

    @Inject
    private LoginBean login;

    private Long tipoUsuarioId;

    public List<Tbtipousuario> getTipoUsuarios() {
        return tipoUsuarioDao.listaTodos();
    }

    @Transacional
    public void gravar() {
        if (podeAtualizarUsuario()) {
            if (usuario.getIdUsuario() == null) {
                dao.adiciona(usuario);
            } else {
                gravarTipoUsuario();
                dao.atualiza(usuario);
            }
            limparUsuario();
        }
    }

    @Transacional
    public void gravarAluno() {
        if (usuario.getIdUsuario() == null) {
            usuario.setTbTipoUsuarioidTipoUsuario(dao.buscarTipo());
            dao.adiciona(usuario);
        }
        limparUsuario();
    }

    @Transacional
    public void salvar() {
        if (podeAtualizarUsuario() && usuario.getTbTipoUsuarioidTipoUsuario().getIdTipoUsuario() == 2) {
            if (usuario.getIdUsuario() == null) {
                dao.adiciona(usuario);
            } else {
                dao.atualiza(usuario);
            }
            limparUsuario();
        }
    }

    @Transacional
    public void remover(Tbusuario usuario) {
        dao.remove(usuario);
    }

    public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow header = sheet.getRow(0);

        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            HSSFCell cell = header.getCell(i);

            cell.setCellStyle(cellStyle);
        }
    }

    public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.open();
        pdf.setPageSize(PageSize.A4);

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String logo = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "images" + File.separator + "uniacademia.png";

        pdf.add(Image.getInstance(logo));
    }

    public void gerarRelatorio() throws IOException {
        Relatorio relatorio = new Relatorio();
        relatorio.getRelatorioAluno(dao.listaTodos());
    }

    public String cadastroAluno() {
        return "cadastroAluno?faces-redirect=true";
    }

    public String voltar() {
        return "login?faces-redirect=true";
    }

    public List<Tbusuario> getUsuarios() {
        return dao.listaTodos();
    }

    public Tbusuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Tbusuario usuario) {
        this.usuario = usuario;
    }

    public void carregar(Tbusuario usuario) {
        this.usuario = usuario;
    }

    public void novo() {
        this.usuario = new Tbusuario();
    }

    public Long getTipoUsuarioId() {
        return tipoUsuarioId;
    }

    public void setTipoUsuarioId(Long tipoUsuarioId) {
        this.tipoUsuarioId = tipoUsuarioId;
    }

    private boolean podeAtualizarUsuario() {
        return login.getUsuarioLogado().getTbTipoUsuarioidTipoUsuario().getIdTipoUsuario() != 2;
    }

    private void gravarTipoUsuario() {
        Tbtipousuario tbtipousuario = tipoUsuarioDao.buscarPorId(tipoUsuarioId);
        usuario.setTbTipoUsuarioidTipoUsuario(tbtipousuario);
    }

    private void limparUsuario() {
        usuario = new Tbusuario();
    }
}
