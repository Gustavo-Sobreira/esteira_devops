package br.com.enade.model;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


class DatabaseConnectionTest {

    private static final String URL = "jdbc:mysql://mysql.prod.svc.cluster.local:3306/enade";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";

    @Test
    void testDatabaseConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            assertNotNull(connection);
            assertFalse(connection.isClosed());
            System.out.println("Conexão com o banco de dados estabelecida com sucesso!");
        } catch (SQLException e) {
            e.printStackTrace();
            fail("Falha ao estabelecer conexão com o banco de dados.");
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}